const express = require("express");
const authRouter = require("./routes/auth.routes");
const userRouter = require("./routes/user.routes");
const userHistoryRouter = require("./routes/userhistory.routes");

const app = express();
const port = 3000;

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use("/", authRouter);
app.use("/user", userRouter);
app.use("/history", userHistoryRouter);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
