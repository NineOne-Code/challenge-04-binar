"use strict";
const { encryption, decryption } = require("../helper/encrypt");
const { User } = require("../models");

module.exports = {
  signIn: async (req, res) => {
    try {
      const { username, password } = req.body;
      const user = await User.findOne({
        attributes: ["id", "username"],
        where: {
          username: username,
          password: password,
        },
      });
      const encrypt = encryption(user.username);
      res.status(200).json({
        message: `User ${user.username} is available, with ID ${user.id}.`,
        token: encrypt,
      });
    } catch (error) {
      res.status(500).json(error);
      console.log(error);
    }
  },
  signUp: async (req, res) => {
    try {
      const { username, password } = req.body;
      const user = await User.create({
        username: username,
        password: password,
      });
      const encrypt = encryption(user.username);
      res.status(200).json({
        message: `Welcome User ${user.username} to We're server, your ID is ${user.id}.`,
        token: encrypt,
      });
    } catch (error) {
      res.status(500).json(error);
      console.log(error);
    }
  },
};
