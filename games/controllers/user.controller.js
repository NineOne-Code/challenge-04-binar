"use strict";
const { decryption } = require("../helper/encrypt");
const { User, UserHistory, UserBiodata } = require("../models");

module.exports = {
  getAllUsers: async (req, res) => {
    try {
      const users = await User.findAll({
        attributes: ["username"],
        include: [
          {
            model: UserBiodata,
            as: "Biodata",
            attributes: ["name", "age", "gender", "level"],
          },
          {
            model: UserHistory,
            as: "Histories",
            attributes: ["time", "score"],
          },
        ],
      });
      res.status(200).json({
        message: `Success get All Players.`,
        data: users,
      });
    } catch (error) {
      res.status(500).json(error);
      console.log(error);
    }
  },
  deleteAccount: async (req, res) => {
    try {
      const username = decryption(req.headers.authorization);
      await User.destroy({
        where: {
          username: username,
        },
      });
      res.status(200).json({
        message: `Success DELETE Account ${username}.`,
      });
    } catch (error) {
      res.status(500).json(error);
      console.log(error);
    }
  },
};
