const { decryption } = require("../helper/encrypt");
const { User, UserHistory, UserBiodata } = require("../models");
module.exports = {
  getUser: async (req, res) => {
    try {
      const username = decryption(req.headers.authorization);
      const user = await User.findOne({
        attributes: ["username"],
        where: {
          username: username,
        },
        include: [
          {
            model: UserBiodata,
            as: "Biodata",
            attributes: ["name", "age", "gender", "level"],
          },
          {
            model: UserHistory,
            as: "Histories",
            attributes: ["time", "score"],
          },
        ],
      });
      res.status(200).json({
        message: `Success get Player.`,
        data: user,
      });
    } catch (error) {
      res.status(500).json(error);
      console.log(error);
    }
  },
  addData: async (req, res) => {
    try {
      const username = decryption(req.headers.authorization);
      const user = await User.findOne({
        attributes: ["id", "username"],
        where: {
          username: username,
        },
      });

      const { name, age, level, gender } = req.body;

      await UserBiodata.create({
        // attributes: ["name", "age", "level", "gender"],
        name: name,
        age: +age,
        level: +level,
        gender: gender,
        userId: user.id,
      });
      res.status(200).json({
        message: `Success add Data Player.`,
      });
    } catch (error) {
      res.status(500).json(error);
      console.log(error);
    }
  },
  updateData: async (req, res) => {
    try {
      const username = decryption(req.headers.authorization);

      const user = await User.findOne({
        attributes: ["id", "username"],
        where: {
          username: username,
        },
      });

      const { name, age, gender, level } = req.body;
      await UserBiodata.update(
        {
          name: name,
          age: age,
          gender: gender,
          level: level,
        },
        {
          where: {
            userId: user.id,
          },
          returning: true,
        }
      );
      res.status(200).json({
        message: `Success Update data Player.`,
      });
    } catch (error) {
      res.status(500).json(error);
      console.log(error);
    }
  },
  deleteData: async (req, res) => {
    try {
      const username = decryption(req.headers.authorization);
      const user = await User.findOne({
        attributes: ["id", "username"],
        where: {
          username: username,
        },
      });

      await UserBiodata.destroy({
        where: {
          userId: user.id,
        },
      });
      res.status(200).json({
        message: `Success delete Data Player.`,
      });
    } catch (error) {
      res.status(500).json(error);
      console.log(error);
    }
  },
};
