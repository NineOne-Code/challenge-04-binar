const { decryption } = require("../helper/encrypt");
const { User, UserHistory, UserBiodata } = require("../models");
module.exports = {
  getAllHistories: async (req, res) => {
    try {
      const histories = await UserHistory.findAll({
        attributes: ["time", "score"],
        include: {
          model: User,
          attributes: ["username"],
        },
      });
      res.status(200).json({
        message: `Success get All Histories.`,
        data: histories,
      });
    } catch (error) {
      res.status(500).json(error);
      console.log(error);
    }
  },
  getHistory: async (req, res) => {
    try {
      const username = decryption(req.headers.authorization);
      const user = await User.findOne({
        attributes: ["id", "username"],
        where: {
          username: username,
        },
      });
      const histories = await UserHistory.findAll({
        attributes: ["time", "score"],
        where: {
          userId: user.id,
        },
      });
      res.status(200).json({
        message: `Success get Your Histories.`,
        data: histories,
      });
    } catch (error) {
      res.status(500).json(error);
      console.log(error);
    }
  },
  addHistory: async (req, res) => {
    try {
      const { score } = req.body;
      const username = decryption(req.headers.authorization);
      const user = await User.findOne({
        attributes: ["id", "username"],
        where: {
          username: username,
        },
      });
      await UserHistory.create({
        score: score,
        time: new Date().toLocaleTimeString().split(" ")[0],
        userId: user.id,
      });
      res.status(200).json({
        message: `Success add Your Record.`,
      });
    } catch (error) {
      res.status(500).json(error);
      console.log(error);
    }
  },
  updateHistory: async (req, res) => {
    try {
      const { id } = req.params;
      const { score } = req.body;
      const username = decryption(req.headers.authorization);
      const user = await User.findOne({
        attributes: ["id", "username"],
        where: {
          username: username,
        },
      });
      await UserHistory.update(
        {
          score: score,
          time: new Date().toLocaleTimeString().split(" ")[0],
        },
        {
          where: {
            id: id,
            userId: user.id,
          },
        }
      );
      res.status(200).json({
        message: `Success update Your Record.`,
      });
    } catch (error) {
      res.status(500).json(error);
      console.log(error);
    }
  },
  deleteHistory: async (req, res) => {
    try {
      const { id } = req.params;
      await UserHistory.destroy({
        where: {
          id: id,
        },
      });
      res.status(200).json({
        message: `Success delete Your Record.`,
      });
    } catch (error) {
      res.status(500).json(error);
      console.log(error);
    }
  },
};
