const express = require("express");
const {
  getAllHistories,
  getHistory,
  addHistory,
  updateHistory,
  deleteHistory,
} = require("../controllers/userhistory.controller");
const { authorization } = require("../middlewares/auth");

var router = express.Router();

router.use(authorization);

router.get("/", getAllHistories);
router.post("/", addHistory);
router.get("/myrecord", getHistory);
router.put("/myrecord/:id", updateHistory);
router.delete("/myrecord/:id", deleteHistory);

module.exports = router;
